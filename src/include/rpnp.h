#pragma once
#include <opencv2/opencv.hpp>
#include <eigen3/Eigen/Dense>
#include <eigen3/unsupported/Eigen/Polynomials>
#include <fstream>
#include <iostream>


using namespace cv;
using namespace Eigen;
using namespace std;

struct rot_trans{
    Matrix3f R;
    RowVector3f t;
};

struct rot_trans_res{
    Matrix3f R;
    RowVector3f t;
    float res;
};

class rpnp
{
private:
    rot_trans calcampose(MatrixXf XXc, MatrixXf XXw);


public:
    MatrixXf objectPoints;
    bool fileInitialized;
    MatrixXf normalizePoints(MatrixXf xx);
    MatrixXf points2Eigen(vector<Point2f> v);
    MatrixXf points2Eigen3D(vector<Point3f> v);
    void eigen2Mat(MatrixXf m, Mat *n);
    rot_trans RPnP(MatrixXf XX, MatrixXf xx);
    rpnp(void);
    ~rpnp(void);
};

