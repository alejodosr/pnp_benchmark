#include "rpnp.h"

#define WIDTH 640
#define HEIGHT 480
#define C_U WIDTH/2
#define C_V HEIGHT/2
#define FOCAL_LENGTH 457.849998 // Simulation
//#define FOCAL_LENGTH 631.632937 // USB CAM


using namespace std;

/// ************** Ceil *************
///
///
template<typename T>
T ceilM(T s){
    T m = s;
    int N = m.rows();
    int M = m.cols();
    for (int i=0; i<N; i++){
        for(int j=0; j<M; j++){
            m(i,j) = ceil(m(i,j));
        }
    }
    return m;
}

/// ************** Round *************
///
///
template<typename T>
T roundM(T s){
    T m = s;
    int N = m.rows();
    int M = m.cols();
    for (int i=0; i<N; i++){
        for(int j=0; j<M; j++){
            m(i,j) = round(m(i,j));
        }
    }
    return m;
}

/// ************** Floor *************
///
///
template<typename T>
T floorM(T s){
    T m = s;
    int N = m.rows();
    int M = m.cols();
    for (int i=0; i<N; i++){
        for(int j=0; j<M; j++){
            m(i,j) = floor(m(i,j));
        }
    }
    return m;
}

/// *************** xCross **************
///
///
//function c = xcross(a,b)

//c = [a(2)*b(3)-a(3)*b(2);
//     a(3)*b(1)-a(1)*b(3);
//     a(1)*b(2)-a(2)*b(1)];

// return
RowVector3f xcross(RowVector3f a, RowVector3f b){
    RowVector3f c;
    c(0) = a(1) * b(2) - a(2) * b(1);
    c(1) = a(2) * b(0) - a(0) * b(2);
    c(2) = a(0) * b(1) - a(1) * b(0);

    return c;
}

/// *************** MATLAB-like repmat **************
///
///
MatrixXf repmat(RowVectorXf r, int M, int N){
    MatrixXf rep;
    rep = r.replicate(M, N);
    return rep;
}

/// *************** MATLAB-like sum **************
///
///
MatrixXf sum(MatrixXf b){
    MatrixXf m(1, b.cols());
    for(int i=0; i<b.cols(); i++){
        m(i) = b.col(i).sum();
    }
    return m;
}

/// *************** Matrix POW **************
///
///
MatrixXf pow(MatrixXf b, double n){
    MatrixXf m(b.rows(), b.cols());
    for (int i=0; i<b.rows(); i++){
        for (int j=0; j<b.cols(); j++){
            m(i,j) = pow(b(i,j), n);
        }
    }
    return m;
}

/// *************** Matrix SQRT **************
///
///
MatrixXf sqrt(MatrixXf b){
    MatrixXf m(b.rows(), b.cols());
    for (int i=0; i<b.rows(); i++){
        for (int j=0; j<b.cols(); j++){
            m(i,j) = sqrt(b(i,j));
        }
    }
    return m;
}

/// *************** Matrix Division by Float **************
///
///
MatrixXf div(MatrixXf b, float f){
    MatrixXf m = b;
    for (int i=0; i<m.rows(); i++){
        for (int j=0; j<m.cols(); j++){
            m(i,j) = m(i,j) / f;
        }
    }
    return m;
}

/// *************** Division Element-by-Element *************
///
///

MatrixXf cwiseDivision(MatrixXf a, MatrixXf b){
    MatrixXf c(a.rows(), b.cols());

    for (int i = 0; i<a.rows(); i++){
        for(int j = 0; j<a.cols(); j++){
            c(i, j) = a(i,j) / b(i, j);
        }
    }

    return c;
}

/// *************** Camera Pose Calculation ***************
///
///

rot_trans rpnp::calcampose(MatrixXf XXc, MatrixXf XXw){
    int n = XXc.cols();

    MatrixXf X = XXw;
    MatrixXf Y = XXc;

    MatrixXf K = MatrixXf::Identity(n,n) - MatrixXf::Ones(n, n) / n;

    RowVector3f ux, uy;
    ux << X.row(0).mean(), X.row(1).mean(), X.row(2).mean();
    uy << Y.row(0).mean(), Y.row(1).mean(), Y.row(2).mean();

    float sigmx2 = sum(pow(X*K, 2)).mean();

    MatrixXf SXY = Y * K * X.transpose() / n;

    JacobiSVD<MatrixXf> svd(SXY, ComputeThinU | ComputeThinV);
    MatrixXf D = MatrixXf::Identity(SXY.rows(), SXY.cols());
    MatrixXf U = svd.matrixU() * -1;    // (-1) To be the same as the MATLAB function
    MatrixXf V = svd.matrixV() * -1;
    D(0,0) = svd.singularValues()(0);
    D(1,1) = svd.singularValues()(1);
    D(2,2) = svd.singularValues()(2);

    MatrixXf S = MatrixXf::Identity(3, 3);

    if(SXY.determinant() < 0){
        S(2, 2) = -1;
    }

    MatrixXf R2 = U * S * V.transpose();
    float c2 = (D * S).trace() / sigmx2;
    RowVectorXf t2 = uy - (c2 * R2 * ux.transpose()).transpose();

    RowVectorXf X_rv;
    RowVectorXf Y_rv;
    RowVectorXf Z_rv;
    X_rv = R2.col(0).transpose();
    Y_rv = R2.col(1).transpose();
    Z_rv = R2.col(2).transpose();

    if(((xcross(X_rv, Y_rv) - Z_rv)).norm() > 2E-2){
        R2.col(2) = -(Z_rv.transpose());
    }

    rot_trans rt;

    rt.R = R2;
    rt.t = t2;

    return rt;

}

/// *************** RPnP Algorithm **************
///
///
rot_trans rpnp::RPnP(MatrixXf XX, MatrixXf xx){
    // Temporal variables
    RowVector3f v3f;
    RowVector3f rv3f;
    RowVector3f rv3f_2;

    rot_trans_res rtr;
    vector<rot_trans_res> vrtr;

    //    n= length(xx);
    //    XXw= XX;
    int n;
    n = xx.cols();
    MatrixXf XXw = XX;

    //    xxv= [xx; ones(1,n)];
    //    for i=1:n
    //        xxv(:,i)= xxv(:,i)/norm(xxv(:,i));
    //    end
    MatrixXf xxv(3, n);
    for (int i=0; i<n; i++){
        v3f(0) = xx.col(i)(0);
        v3f(1) = xx.col(i)(1);
        v3f(2) = 1;
        xxv.col(i) = (v3f / v3f.norm()).transpose();
    }



    /// Selecting an edge $P_{i1}p_{i2}$ by n random sampling
    ///
    ///
    //    i1= 1;
    //    i2= 2;
    //    lmin= xxv(1,i1)*xxv(1,i2) + xxv(2,i1)*xxv(2,i2) + xxv(3,i1)*xxv(3,i2);
    int i1 = 0;
    int i2 = 1;
    float lmin = xxv.col(i1)(0) * xxv.col(i2)(0) + xxv.col(i1)(1) * xxv.col(i2)(1) + xxv.col(i1)(2) * xxv.col(i2)(2);

    //    rij= ceil(rand(n,2)*n);
    MatrixXf rij = MatrixXf::Random(n,2);
    rij = rij.cwiseAbs() * n;
    rij = floorM<MatrixXf>(rij);

    //    for ii= 1:n
    //        i= rij(ii,1);
    //        j= rij(ii,2);
    //        if i == j
    //            continue;
    //        end
    //        l= xxv(1,i)*xxv(1,j)+xxv(2,i)*xxv(2,j)+xxv(3,i)*xxv(3,j);
    //        if l < lmin
    //            i1= i;
    //            i2= j;
    //            lmin= l;
    //        end
    //    end
    int i;
    int j;
    float l;
    for(int ii = 0; ii<n; ii++){
        i = rij(ii, 0);
        j = rij(ii, 1);
        if (i == j)   continue;
        l = xxv.col(i)(0) * xxv.col(j)(0) + xxv.col(i)(1) * xxv.col(j)(1) + xxv.col(i)(2) * xxv.col(j)(2);
        if (l < lmin){
            i1 = i;
            i2 = j;
            lmin = l;
        }
    }

    // Only for debugging purposes
    i1 = 2;
    i2 = 1;
    lmin = 0.9517;

    /// Calculating the rotation matrix of $O_aX_aY_aZ_a$.
    ///
    ///

    //    p1= XX(:,i1);
    //    p2= XX(:,i2);
    //    p0= (p1+p2)/2;x= p2-p0; x= x/norm(x);
    //    if abs([0 1 0]*x) < abs([0 0 1]*x)
    //        z= xcross(x,[0; 1; 0]); z= z/norm(z);
    //        y= xcross(z, x); y= y/norm(y);
    //    else
    //        y= xcross([0; 0; 1], x); y= y/norm(y);
    //        z= xcross(x,y); z= z/norm(z);
    //    end
    //    Ro= [x y z];
    RowVector3f p1 = XX.col(i1).transpose();
    RowVector3f p2 = XX.col(i2).transpose();
    RowVector3f p0 = (p1 + p2) / 2;

    RowVector3f x = p2 -p0;
    RowVector3f y;
    RowVector3f z;
    x = x / x.norm();
    rv3f << 0, 1, 0;
    rv3f_2 << 0, 0, 1;

    if(((rv3f * x.transpose()).cwiseAbs())(0) < ((rv3f_2 * x.transpose()).cwiseAbs()(0))){
        z = xcross(x, rv3f);
        z = z / z.norm();
        y = xcross(z, x);
        y = y / y.norm();
    }
    else{
        y = xcross(rv3f_2, x);
        y = y / y.norm();
        z = xcross(x, y);
        z = z / z.norm();
    }

    Matrix3f Ro;
    Ro.row(0) = x;
    Ro.row(1) = y;
    Ro.row(2) = z;
    MatrixXf Ro_temp = Ro.transpose();
    Ro = Ro_temp;

    /// Transforming the reference points from original object space
    /// to the new coordinate frame $O_aX_aY_aZ_a$.
    ///
    ///
    //    XX= Ro.'*(XX-repmat(p0,1,n))
    XX = Ro.transpose() * (XX -repmat(p0, n, 1).transpose());   // Not taking into account the .'* MATLAB product, only '*

    /// Dividing the n-point set into (n-2) 3-point subsets
    /// and setting up the P3P equations
    ///
    ///
    //    v1= xxv(:,i1);
    //    v2= xxv(:,i2);
    //    cg1= v1.'*v2;
    //    sg1= sqrt(1-cg1^2);
    //    D1= norm(XX(:,i1)-XX(:,i2));
    //    D4= zeros(n-2,5);
    RowVector3f v1 = xxv.col(i1).transpose();
    RowVector3f v2 = xxv.col(i2).transpose();
    float cg1 = (v1 * v2.transpose())(0);
    float sg1 = sqrt(1 - pow(cg1, 2));
    float D1 = (XX.col(i1) - XX.col(i2)).norm();
//    MatrixXf D4 = MatrixXf::Zero(n-2, 5);

    //    idx= true(1,n);
    //    idx([i1 i2])= false;
    //    vi= xxv(:,idx);
    //    cg2= vi.'*v1;
    //    cg3= vi.'*v2;
    //    sg2= sqrt(1-cg2.^2);
    //    D2= cg2;
    //    D3= cg2;
    //    didx= find(idx);
    //    for i= 1:n-2
    //        D2(i)= norm(XX(:,i1)-XX(:,didx(i)));
    //        D3(i)= norm(XX(:,didx(i))-XX(:,i2));
    //    end

    //    A1= (D2./D1).^2;
    //    A2= A1*sg1^2-sg2.^2;
    //    A3= cg2.*cg3-cg1;
    //    A4= cg1*cg3-cg2;
    //    A6= (D3.^2-D1^2-D2.^2)./(2*D1^2);
    //    A7= 1-cg1^2-cg2.^2+cg1*cg2.*cg3+A6.*sg1^2;

    //    D4= [A6.^2-A1.*cg3.^2, 2*(A3.*A6-A1.*A4.*cg3),...
    //        A3.^2+2*A6.*A7-A1.*A4.^2-A2.*cg3.^2,...
    //        2*(A3.*A7-A2.*A4.*cg3), A7.^2-A2.*A4.^2];

    //    F7= [4*D4(:,1).^2,...
    //        7*D4(:,2).*D4(:,1),...
    //        6*D4(:,3).*D4(:,1)+3*D4(:,2).^2,...
    //        5*D4(:,4).*D4(:,1)+5*D4(:,3).*D4(:,2),...
    //        4*D4(:,5).*D4(:,1)+4*D4(:,4).*D4(:,2)+2*D4(:,3).^2,...
    //        3*D4(:,5).*D4(:,2)+3*D4(:,4).*D4(:,3),...
    //        2*D4(:,5).*D4(:,3)+D4(:,4).^2,...
    //        D4(:,5).*D4(:,4)];
    //    D7= sum(F7);
    RowVectorXf idx = RowVectorXf::Ones(n);
    idx(i1) = 0;
    idx(i2) = 0;
    MatrixXf vi;
    vector<int> didx;
    for(int i=0; i<idx.cols(); i++){
        if(idx(i) == 1){
            didx.push_back(i);
            vi.conservativeResizeLike(MatrixXf::Ones(3, vi.cols() + 1));
            vi.col(vi.cols() - 1) = xxv.col(i);
        }
    }
    MatrixXf cg2 = vi.transpose() * v1.transpose();
    MatrixXf cg3 = vi.transpose() * v2.transpose();
    MatrixXf sg2 = sqrt(MatrixXf::Ones(cg2.rows(), cg2.cols()) - pow(cg2, 2));

    MatrixXf D2 = cg2;
    MatrixXf D3 = cg2;
    //    int temp;
    //    int k;
    for(int i=0; i<n-2; i++){
        //        temp = i;
        //        j = 0;
        //        k = 0;
        //        while(temp>0){
        //            if(j == D2.rows() - 1){
        //                j = 0;
        //                k++;
        //            }
        //            j++;
        //            temp--;
        //        }
        D2(i) = (XX.col(i1) - XX.col(didx[i])).norm();
        D3(i) = (XX.col(didx[i]) - XX.col(i2)).norm();
    }
    MatrixXf A1 = pow(div(D2, D1), 2);
    MatrixXf A2 = A1*pow(cg1, 2) - pow(sg2, 2);
    MatrixXf A3 = cg2.cwiseProduct(cg3) - MatrixXf::Ones(cg2.rows(), cg2.cols()) * cg1;
    MatrixXf A4 = cg1 * cg3 - cg2;
    MatrixXf A6 = div((pow(D3, 2) - MatrixXf::Ones(D3.rows(), D3.cols()) * pow(D1, 2) - pow(D2, 2)), (2 * pow(D1, 2)));
    MatrixXf A7 = (MatrixXf::Ones(cg3.rows(), cg3.cols()) * (1 - pow(cg1, 2))) - pow(cg2, 2) + (cg1 * cg2).cwiseProduct(cg3) + A6 * (pow(sg1, 2));

    MatrixXf T1 = pow(A6, 2) - A1.cwiseProduct(pow(cg3, 2));
    MatrixXf T2 = 2 * (A3.cwiseProduct(A6) - A1.cwiseProduct(A4).cwiseProduct(cg3));
    MatrixXf T3 = pow(A3, 2) + 2 * A6.cwiseProduct(A7) - A1.cwiseProduct(pow(A4, 2)) - A2.cwiseProduct(pow(cg3, 2));
    MatrixXf T4 = 2 * (A3.cwiseProduct(A7) - A2.cwiseProduct(A4).cwiseProduct(cg3));
    MatrixXf T5 = pow(A7, 2) - A2.cwiseProduct(pow(A4, 2));
    MatrixXf D4(T1.rows(), T1.cols()+T2.cols() + T3.cols() + T4.cols() + T5.cols());
    D4 << T1, T2, T3, T4, T5;

    T1 = 4 * pow(D4.col(0), 2);
    T2 = 7 * D4.col(1).cwiseProduct(D4.col(0));
    T3 = 6 * D4.col(2).cwiseProduct(D4.col(0)) + 3 * pow(D4.col(1), 2);
    T4 = 5 * D4.col(3).cwiseProduct(D4.col(0)) + 5 * D4.col(2).cwiseProduct(D4.col(1));
    T5 = 4 * D4.col(4).cwiseProduct(D4.col(0)) + 4 * D4.col(3).cwiseProduct(D4.col(2)) + 2 * pow(D4.col(2), 2);
    MatrixXf T6 = 3 * D4.col(4).cwiseProduct(D4.col(1)) + 3 * D4.col(3).cwiseProduct(D4.col(2));
    MatrixXf T7 = 2 * D4.col(4).cwiseProduct(D4.col(2)) + pow(D4.col(3), 2);
    MatrixXf T8 = D4.col(4).cwiseProduct(D4.col(3));
    MatrixXf F7(T1.rows(), T1.cols()+T2.cols() + T3.cols() + T4.cols() + T5.cols() + T6.cols() + T7.cols() + T8.cols());
    F7 << T1, T2, T3, T4, T5, T6, T7, T8;

    MatrixXf D7 = sum(F7);


    /// Retrieving the local minima of the cost function
    ///
    ///

    //        t2s= roots(D7);

    //        maxreal= max(abs(real(t2s)));
    //        t2s(abs(imag(t2s))/maxreal > 0.001)= [];
    //        t2s= real(t2s);

    //        D6= (7:-1:1).*D7(1:7);
    //        F6= polyval(D6,t2s);
    //        t2s(F6 <= 0)= [];

    //        if isempty(t2s)
    //            fprintf('no solution!\n');
    //            return
    //        end

    Eigen::PolynomialSolver<double, Eigen::Dynamic> solver;
    Eigen::VectorXd coeff(D7.cols());

    // Filling coefficients
    for(int i=0; i<D7.cols(); i++){
        coeff(i) = D7(D7.cols() -1 - i);
    }
    solver.compute(coeff);
    const Eigen::PolynomialSolver<double, Eigen::Dynamic>::RootsType & r = solver.roots();



    // Looking for the maximum in real part
    double maxreal = 0;
    double *p = (double *) (r.data());
    for (int i=0; i<r.rows(); i++){
        if(abs(*(p + 2*i)) > maxreal)    maxreal = abs(*(p + 2*i));
    }

    RowVectorXf t2s;
    for (int i=0; i<r.rows(); i++){
        if((abs(*(p + (2*i + 1))) / maxreal) <= 0.001 ){
            t2s.conservativeResizeLike(RowVectorXf::Ones(t2s.cols() + 1));
            t2s(t2s.cols() - 1) = *(p + 2*i);
        }
    }

    cout << t2s << endl;

    MatrixXf tm(1,7);
    tm << 7, 6, 5, 4, 3, 2, 1;
    MatrixXf D6 = tm.cwiseProduct(D7.block<1, 7>(0,0));

    double eval;
    Eigen::Matrix<float,7,1> polynomial = D6.transpose();
    RowVectorXf t2s_new;
    for(int i=0; i<t2s.cols(); i++){
        eval = poly_eval( polynomial, t2s[i]);
        if (eval > 0){
            t2s_new.conservativeResizeLike(RowVectorXf::Ones(t2s_new.cols() + 1));
            t2s_new(t2s_new.cols() - 1) = t2s[i];
        }
    }



    if(t2s_new.cols() == 0){
        cout << "[RPnP ERROR]: solution not found" << endl;
        rot_trans rt;
        return rt;
    }

    /// Calculating the camera pose from each local minimum.
    ///
    ///
//    % calculating the camera pose from each local minimum.
//    m= length(t2s);
//    for i= 1:m
//        t2= t2s(i);
//        % calculating the rotation matrix
//        d2= cg1+t2;
//        x= v2*d2- v1; x= x/norm(x);

//        if abs([0 1 0]*x) < abs([0 0 1]*x)
//            z= xcross(x,[0; 1; 0]); z= z/norm(z);
//            y= xcross(z, x); y= y/norm(y);
//        else
//            y= xcross([0; 0; 1], x); y= y/norm(y);
//            z= xcross(x,y); z= z/norm(z);
//        end
//        Rx= [x y z];

    int m = t2s_new.cols();
    float t2;
    float d2;
    rv3f << 0, 1, 0;
    rv3f_2 << 0, 0, 1;
    Matrix3f Rx;
    MatrixXf D;
    MatrixXf r_m;
    float vi_f, ui_f, xi_f, yi_f, zi_f;
    RowVectorXf xi, yi, zi;
    MatrixXf DTD;
    RowVectorXf V1;
    float c, s;
    MatrixXf XXcs(3, XX.cols());
    MatrixXf XXc;
    Matrix3f R;
    RowVector3f t;

    for (int i = 0; i<m; i++){
        t2 = t2s_new(i);
        // Calculating the rotation matrix
        d2 = cg1 + t2;
        x = v2 * d2 - v1;
        x = x / x.norm();

        if(((rv3f * x.transpose()).cwiseAbs())(0) < ((rv3f_2 * x.transpose()).cwiseAbs()(0))){
            z = xcross(x, rv3f);
            z = z / z.norm();
            y = xcross(z, x);
            y = y / y.norm();
        }
        else{
            y = xcross(rv3f_2, x);
            y = y / y.norm();
            z = xcross(x, y);
            z = z / z.norm();
        }

        Rx.row(0) = x;
        Rx.row(1) = y;
        Rx.row(2) = z;



//        % calculating c, s, tx, ty, tz
//        D= zeros(2*n,6);
//        r= Rx.';
//        for j= 1:n
//            ui= xx(1,j); vi= xx(2,j);
//            xi= XX(1,j); yi= XX(2,j); zi= XX(3,j);
//            D(2*j-1,:)= [-r(2)*yi+ui*(r(8)*yi+r(9)*zi)-r(3)*zi, ...
//                -r(3)*yi+ui*(r(9)*yi-r(8)*zi)+r(2)*zi, ...
//                -1, 0, ui, ui*r(7)*xi-r(1)*xi];
//            D(2*j, :)= [-r(5)*yi+vi*(r(8)*yi+r(9)*zi)-r(6)*zi, ...
//                -r(6)*yi+vi*(r(9)*yi-r(8)*zi)+r(5)*zi, ...
//                0, -1, vi, vi*r(7)*xi-r(4)*xi];
//        end
//        DTD= D.'*D;
//        [V D]= eig(DTD);

//        V1= V(:,1); V1= V1/V1(end);
//        c= V1(1); s= V1(2); t= V1(3:5);

        // Calculating c, s, tx, ty, tz
        D = MatrixXf::Zero(2 * n, 6);
        r_m = Rx;
        cout << "r_m" << endl << r_m << endl;

        for(int j=0; j<n; j++){
            ui_f = xx(0, j);
            vi_f = xx(1, j);
            xi_f = XX(0, j);
            yi_f = XX(1, j);
            zi_f = XX(2, j);

            D.row(2*j) << (-r_m(1) * yi_f + ui_f * (r_m(7) * yi_f + r_m(8) * zi_f) - r_m(2) * zi_f),
                                (-r_m(2) * yi_f + ui_f * (r_m(8) * yi_f - r_m(7) * zi_f) + r_m(1) * zi_f),
                                    -1, 0, ui_f, ui_f * r_m(6) * xi_f - r_m(0) * xi_f;


            D.row(2*j + 1) << (-r_m(4) * yi_f  + vi_f * (r_m(7) * yi_f + r_m(8) * zi_f) - r_m(5) * zi_f),
                                (-r_m(5) * yi_f + vi_f * (r_m(8) * yi_f -r_m(7) * zi_f) + r_m(4) * zi_f),
                                    0, -1, vi_f, (vi_f * r_m(6) * xi_f - r_m(3) * xi_f);
        }

        cout << "D" << endl << D << endl;

        DTD = D.transpose()*D;
        EigenSolver<MatrixXf> eigensolver(DTD);
        MatrixXf V = eigensolver.pseudoEigenvectors();
        MatrixXf D = eigensolver.pseudoEigenvalueMatrix();

        cout << "DTD" << endl << DTD << endl;
        cout << "V" << endl << V << endl;
        cout << "D" << endl << D << endl;

        // Add code to solve eigen solver definciencies
        MatrixXf sum_matrix = sum(D);
        float D_min = 14E12;
        int i_min=-1;
        for(int i = 0; i<sum_matrix.cols(); i++){
            if(sum_matrix(i) < D_min){
                D_min = sum_matrix(i);
                i_min = i;
            }
        }

        V1 = V.col(i_min).transpose();  // MODIFIED!!
        V1 = V1 / V1(V1.cols() - 1);
        c = V1(0);
        s = V1(1);
        cout << V1.block<1, 3>(0, 2) << endl;
        t = V1.block<1, 3>(0, 2);

        /// Calculating the camera pose by 3D alignment
        ///
        ///

//        % calculating the camera pose by 3d alignment
//        xi= XX(1,:); yi= XX(2,:); zi= XX(3,:);
//        XXcs= [r(1)*xi+(r(2)*c+r(3)*s)*yi+(-r(2)*s+r(3)*c)*zi+t(1);
//            r(4)*xi+(r(5)*c+r(6)*s)*yi+(-r(5)*s+r(6)*c)*zi+t(2);
//            r(7)*xi+(r(8)*c+r(9)*s)*yi+(-r(8)*s+r(9)*c)*zi+t(3)];

//        XXc= zeros(size(XXcs));
//        for j= 1:n
//            XXc(:,j)= xxv(:,j)*norm(XXcs(:,j));
//        end

//        [R t]= calcampose(XXc,XXK);

        xi = XX.row(0);
        yi = XX.row(1);
        zi = XX.row(2);

        XXcs.row(0) = r_m(0) * xi + (r_m(1) * c + r_m(2) * s) * yi + (-r_m(1) * s + r_m(2) * c) * zi + RowVectorXf::Ones(XX.cols()) * t(0);
        XXcs.row(1) = r_m(3) * xi + (r_m(4) * c + r_m(5) * s) * yi + (-r_m(4) * s + r_m(5) * c) * zi + RowVectorXf::Ones(XX.cols()) * t(1);
        XXcs.row(2) = r_m(6) * xi + (r_m(7) * c + r_m(8) * s) * yi + (-r_m(7) * s + r_m(8) * c) * zi + RowVectorXf::Ones(XX.cols()) * t(2);

        cout << "XXcs" << endl << XXcs << endl;


        XXc = MatrixXf::Zero(XXcs.rows(), XXcs.cols());

        for (int j = 0; j<n; j++){
            XXc.col(j) = xxv.col(j) * XXcs.col(j).norm();
        }

        rot_trans rt;

        rt = calcampose(XXc, XXw);

        cout << "rt" << endl << rt.R << endl;
        cout << "rt" << endl << rt.t << endl;


        /// Calculating the reprojection error
        ///
        ///

//        % calculating the reprojection error
//        XXc= R*XXw+t*ones(1,n);
//        xxc= [XXc(1,:)./XXc(3,:); XXc(2,:)./XXc(3,:)];
//        r= mean(sqrt(sum((xxc-xx).^2)));

//        res{i}.R= R;
//        res{i}.t= t;
//        res{i}.r= r;

        R = rt.R;
        t = rt.t;

        XXc = R * XXw + t.transpose() * MatrixXf::Ones(1, n);
        MatrixXf xxc(2, n);
        xxc.row(0) = cwiseDivision(XXc.row(0), XXc.row(2));
        xxc.row(1) = cwiseDivision(XXc.row(1), XXc.row(2));

        float res = sqrt(sum(pow((xxc-xx), 2))).mean();

        rtr.R = R;
        rtr.t = t;
        rtr.res = res;

        vrtr.push_back(rtr);
    }

    /// Determining the camera pose with the smallest reprojection error
    ///
    ///

//    % determing the camera pose with the smallest reprojection error.
//    minr= inf;
//    for i= 1:m
//        if res{i}.r < minr
//            minr= res{i}.r;
//            R= res{i}.R;
//            t= res{i}.t;
//        end
//    end

    float minr = 14E15;
    for (int i = 0; i<vrtr.size(); i++){
        if(vrtr[i].res < minr){

            minr = vrtr[i].res;
            R = vrtr[i].R;
            t = vrtr[i].t;
        }
    }

    rot_trans rt;
    rt.R = R;
    rt.t = t;

    return rt;

}

/// ************ Normalize Points *****************
///
///

MatrixXf rpnp::normalizePoints(MatrixXf xx){
    MatrixXf xxn(xx.rows(), xx.cols());

    xxn.row(0) = (xx.row(0) - RowVectorXf::Ones(xx.cols()) * C_U) / FOCAL_LENGTH;
    xxn.row(1) = (xx.row(1) - RowVectorXf::Ones(xx.cols()) * C_V) / FOCAL_LENGTH;

    return xxn;
}

/// ************ Points to Eigen *******************
///
///

MatrixXf rpnp::points2Eigen(vector<Point2f> v){
    MatrixXf m(2, v.size());

    for(int i=0; i<v.size(); i++){
        m(0, i) = v[i].x;
        m(1, i) = v[i].y;
    }

    return m;
}

/// ************ Points to Eigen (3D) *******************
///
///

MatrixXf rpnp::points2Eigen3D(vector<Point3f> v){
    MatrixXf m(3, v.size());

    for(int i=0; i<v.size(); i++){
        m(0, i) = v[i].x;
        m(1, i) = v[i].y;
        m(2, i) = v[i].z;
    }

    return m;
}

/// ************ Eigen to cv::Mat ******************
///
///

void rpnp::eigen2Mat(MatrixXf m, Mat *n){
    int rows = m.rows();
    int cols = m.cols();
    Mat z(rows, cols, DataType<double>::type);
    for(int i=0; i<m.rows(); i++){
        for(int j=0; j<m.cols();j++){
            z.at<double>(i,j) = m(i,j);
        }
    }
    z.copyTo(*n);
}

/// ************ Constructor and Destructor **********
///
///

rpnp::rpnp(){
}
rpnp::~rpnp(){}
