
#include "opencv2/opencv.hpp"
//#include "opencv2/nonfree/features2d.hpp"
#include "CreadorMascaras.h"
#include "Hough.h"
#include "LineDetector.h"
#include "ObjectDetector.h"
#include "PoseEstimator.h"
#include "ColorImageProcessor.h"
#include "rpnp.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"

//#define ORIGIN_WIN_GAZEBO_X     3.03644
//#define ORIGIN_WIN_GAZEBO_Y     -6.07064
//#define ORIGIN_WIN_GAZEBO_Z     2.07112
#define ORIGIN_WIN_GAZEBO_X     12.529359
#define ORIGIN_WIN_GAZEBO_Y     -2.9544
#define ORIGIN_WIN_GAZEBO_Z     2.00403
#define SATURATION_SCARA    20.0

using namespace std;


std::vector<cv::Point3f> Read3DObjectPoints(std::vector<cv::Point2f> &image_corners, int object_type);
std::vector<cv::Point3f> Read3DProjectedPoints(int object_type);
void imageCallback(const sensor_msgs::ImageConstPtr& msg);
void scaraCallback(const geometry_msgs::PoseWithCovarianceStampedConstPtr& msg);

ros::Publisher RPnPpub;
ros::Publisher OpenCVPnPpub;
ros::Publisher Scarapub2;
image_transport::Publisher Scarapub;



int main(int argc, char **argv)
{
    //ROS init
    ros::init(argc, argv, "pose_estimation_node");
    ros::NodeHandle n;
    std::cout << "[ROSNODE] Starting" << ros::this_node::getName() << std::endl;

    image_transport::ImageTransport it(n);
    image_transport::Subscriber sub = it.subscribe("/camera_cam/image_raw", 1, imageCallback);
    ros::Subscriber Scarasub = n.subscribe("/monocular_pose_estimator/estimated_pose", 1000, scaraCallback);

    RPnPpub = n.advertise<geometry_msgs::Pose>("vb_estimated_pose/rpnp_pose", 1000);
    OpenCVPnPpub = n.advertise<geometry_msgs::Pose>("vb_estimated_pose/opencv_pnp_pose", 1000);
    Scarapub2 = n.advertise<geometry_msgs::Pose>("vb_estimated_pose/scara_pose", 1000);
    Scarapub = it.advertise("camera/image_scara", 1);

    try
    {
        ros::spin();
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }

    return 0;
}

void scaraCallback(const geometry_msgs::PoseWithCovarianceStampedConstPtr& msg){
    geometry_msgs::Pose pose, temp;

    /// Transform from Camera Frame of Reference (FoR) to Gazebo FoR
    ///
    // Scaramuzza (from camera FoR to window FoR)
    pose.position.x = - msg->pose.pose.position.x;
    pose.position.y = - msg->pose.pose.position.y;
    pose.position.z = - msg->pose.pose.position.z;

    // Scaramuzza (from window FoR to Gazebo FoR)
    temp = pose;

    pose.position.x = temp.position.x;
    pose.position.y = temp.position.z;
    pose.position.z = - temp.position.y;

    /// Controlling spikes
    ///
    if(pose.position.x > SATURATION_SCARA)    pose.position.x = SATURATION_SCARA;
    if(pose.position.y > SATURATION_SCARA)    pose.position.y = SATURATION_SCARA;
    if(pose.position.z > SATURATION_SCARA)    pose.position.z = SATURATION_SCARA;

    if(pose.position.x < -SATURATION_SCARA)    pose.position.x = -SATURATION_SCARA;
    if(pose.position.y < -SATURATION_SCARA)    pose.position.y = -SATURATION_SCARA;
    if(pose.position.z < -SATURATION_SCARA)    pose.position.z = -SATURATION_SCARA;

    Scarapub2.publish(pose);
}

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  try
  {
    cv::imshow("view", cv_bridge::toCvShare(msg, "bgr8")->image);
    ObjectDetector objectDetector;
    PoseEstimator poseEstimator;
    ColorImageProcessor colorImageProcessor;
    cv::VideoWriter writer;

    cv::Mat I, I_canny, I_hsv;
    I = cv_bridge::toCvShare(msg, "bgr8")->image;

    cv::Mat I_for_hough, I_for_color, I_for_pose;
    cv::Mat I_OP_keypoints, I_OP_binarized;
    I.copyTo(I_for_hough);
    I.copyTo(I_for_color);
    I.copyTo(I_for_pose);

    //colorImageProcessor.ProcessInLabColorSpace(I_for_color);
    //colorImageProcessor.ProcessInHLSColorSpace(I_for_color);
    colorImageProcessor.ProcessInOpponentColorSpace(I_for_color);

    bool detect_door_and_window = true;


    rpnp rPNP = rpnp();
    cv::Mat Rmat_RPnP;
    cv::Mat Tmat_RPnP;
    cv::Mat Rmat;
    cv::Mat Tmat;

    /// Detect 2D corners
    ///
    std::vector<cv::Point2f> door_corners_2D;
    door_corners_2D = objectDetector.DetectDoorCorners(I_for_color);
    std::vector<cv::Point2f> window_corners_2D;
    window_corners_2D = objectDetector.DetectWindowCorners(I_for_color);

    /// Publish Scamaramuzza-algorithm image input
    ///

    Mat I_scara(I_for_color.rows, I_for_color.cols, CV_8UC3, Scalar(0,0,0));

    for(int i=0;i<door_corners_2D.size();i++)
    {
        cv::circle(I_scara, door_corners_2D[i], 6, cv::Scalar(255, 255, 255), -2);
    }

    for(int i=0;i<window_corners_2D.size();i++)
    {
        cv::circle(I_scara, window_corners_2D[i], 6, cv::Scalar(255, 255, 255), -2);
    }

    sensor_msgs::ImagePtr msg_scara = cv_bridge::CvImage(std_msgs::Header(), "bgr8", I_scara).toImageMsg();
    msg_scara->header.stamp = msg->header.stamp;
    Scarapub.publish(msg_scara);

    if((window_corners_2D.size() == 4) && (door_corners_2D.size() == 4))
   {

       /// Read 3D Object coordinates
       ///
       std::vector<cv::Point3f> corners_3D = Read3DObjectPoints(door_corners_2D, 6);

       /// Match 3D coordinates to 2D coordinates
       ///
       // Door
       std::vector<cv::Point2f> door_corners_2D_sorted;
       MatrixXf doorImagePoints(2, 4);
       door_corners_2D_sorted = poseEstimator.matchOBjectToImageRansac(door_corners_2D);
       doorImagePoints = rPNP.points2Eigen(door_corners_2D_sorted);
       // Window
       std::vector<cv::Point2f> window_corners_2D_sorted;
       MatrixXf windowImagePoints(2, 4);
       window_corners_2D_sorted = poseEstimator.matchOBjectToImageRansac(window_corners_2D);
       if((window_corners_2D_sorted.size() != 0) && (door_corners_2D_sorted.size() != 0)){    // Sorting algorithm did not fail
           windowImagePoints = rPNP.points2Eigen(window_corners_2D_sorted);
           // Joining corners
           MatrixXf ImagePoints(2, 8);
           ImagePoints.block<2,4>(0,0) = windowImagePoints;
           ImagePoints.block<2,4>(0,4) = doorImagePoints;

           std::vector<cv::Point2f> corners_2D;
           corners_2D.push_back(window_corners_2D_sorted[0]);
           corners_2D.push_back(window_corners_2D_sorted[1]);
           corners_2D.push_back(window_corners_2D_sorted[2]);
           corners_2D.push_back(window_corners_2D_sorted[3]);
           corners_2D.push_back(door_corners_2D_sorted[0]);
           corners_2D.push_back(door_corners_2D_sorted[1]);
           corners_2D.push_back(door_corners_2D_sorted[2]);
           corners_2D.push_back(door_corners_2D_sorted[3]);


           /// Estimate Pose
           ///
           // OpenCV PnP Estimator
           poseEstimator.EstimatePoseFromPNP(corners_3D, corners_2D);
           // RPnp Estimator
           MatrixXf ImagePointsNormalized(2, 8);
           ImagePointsNormalized = rPNP.normalizePoints(ImagePoints);
           MatrixXf ObjectPoints = rPNP.points2Eigen3D(corners_3D);
           rot_trans rt = rPNP.RPnP(ObjectPoints, ImagePointsNormalized);

           /// Read points to project
           ///
           std::vector<cv::Point3f> points_3D_for_project = Read3DProjectedPoints(5);

           /// Project axes and display info
           ///
           // OpenCV PnP
           Rmat = poseEstimator.getOrientation();
           Tmat = poseEstimator.getTranslation();
           poseEstimator.Project3Dpoints(I, "Projected Axis (OpenCV PnP)", points_3D_for_project, Rmat, Tmat, "Door + Window");

           // RPnP
           rPNP.eigen2Mat(rt.R, &Rmat_RPnP);
           cv::Mat Rvec;
           Rodrigues(Rmat, Rvec);
           rPNP.eigen2Mat(rt.t.transpose(), &Tmat_RPnP);
           cout << "Tmat" << Tmat_RPnP << endl;
           poseEstimator.Project3Dpoints(I, "Projected Axis (RPnP)", points_3D_for_project, Rmat_RPnP, Tmat_RPnP, "Door + Window");
       }
       else{
           door_corners_2D.clear();
           window_corners_2D.clear();
       }
    }

    else if(door_corners_2D.size() == 4)
    {

        /// Read 3D Object coordinates
        ///
        std::vector<cv::Point3f> door_corners_3D = Read3DObjectPoints(door_corners_2D, 8);

        /// Match 3D coordinates to 2D coordinates
        ///
        std::vector<cv::Point2f> door_corners_2D_sorted;
        MatrixXf doorImagePoints(2, 4);
        door_corners_2D_sorted = poseEstimator.matchOBjectToImageRansac(door_corners_2D);
        if(door_corners_2D_sorted.size() != 0){    // Sorting algorithm did not fail
            doorImagePoints = rPNP.points2Eigen(door_corners_2D_sorted);

            /// Estimate Pose
            ///
            // OpenCV PnP Estimator
            poseEstimator.EstimatePoseFromPNP(door_corners_3D, door_corners_2D_sorted);
            // RPnp Estimator
            MatrixXf doorImagePointsNormalized(2, 4);
            doorImagePointsNormalized = rPNP.normalizePoints(doorImagePoints);
            MatrixXf doorObjectPoints = rPNP.points2Eigen3D(door_corners_3D);
            rot_trans rt = rPNP.RPnP(doorObjectPoints, doorImagePointsNormalized);

            /// Read points to project
            ///
            std::vector<cv::Point3f> points_3D_for_project = Read3DProjectedPoints(5);

            /// Project axes and display info
            ///
            // OpenCV PnP
            Rmat = poseEstimator.getOrientation();
            Tmat = poseEstimator.getTranslation();
            poseEstimator.Project3Dpoints(I, "Projected Axis (OpenCV PnP)", points_3D_for_project, Rmat, Tmat, "Door");

            // RPnP
            rPNP.eigen2Mat(rt.R, &Rmat_RPnP);
            cv::Mat Rvec;
            Rodrigues(Rmat, Rvec);
            rPNP.eigen2Mat(rt.t.transpose(), &Tmat_RPnP);
            cout << "Tmat" << Tmat_RPnP << endl;
            poseEstimator.Project3Dpoints(I, "Projected Axis (RPnP)", points_3D_for_project, Rmat_RPnP, Tmat_RPnP, "Door");
        }
        else door_corners_2D.clear();
    }

    else if(window_corners_2D.size() == 4)
    {

        /// Read 3D Object coordinates
        ///
        std::vector<cv::Point3f> window_corners_3D = Read3DObjectPoints(window_corners_2D, 3);

        /// Match 3D coordinates to 2D coordinates
        ///
        std::vector<cv::Point2f> window_corners_2D_sorted;
        MatrixXf windowImagePoints(2, 4);
        window_corners_2D_sorted = poseEstimator.matchOBjectToImageRansac(window_corners_2D);
        if(window_corners_2D_sorted.size() != 0){    // Sorting algorithm did not fail
            windowImagePoints = rPNP.points2Eigen(window_corners_2D_sorted);

            /// Estimate Pose
            ///
            // OpenCV PnP Estimator
            poseEstimator.EstimatePoseFromPNP(window_corners_3D, window_corners_2D_sorted);
            // RPnp Estimator

            MatrixXf windowImagePointsNormalized(2, 4);
            windowImagePointsNormalized = rPNP.normalizePoints(windowImagePoints);
            MatrixXf windowObjectPoints = rPNP.points2Eigen3D(window_corners_3D);
            rot_trans rt = rPNP.RPnP(windowObjectPoints, windowImagePointsNormalized);

            /// Read points to project
            ///
            std::vector<cv::Point3f> points_3D_for_project = Read3DProjectedPoints(6);

            /// Project axes and display info
            ///
            // OpenCV PnP
            Rmat = poseEstimator.getOrientation();
            Tmat = poseEstimator.getTranslation();
            poseEstimator.Project3Dpoints(I, "Projected Axis (OpenCV PnP)", points_3D_for_project, Rmat, Tmat, "Window");

            // RPnP
            rPNP.eigen2Mat(rt.R, &Rmat_RPnP);
            cv::Mat Rvec;
            Rodrigues(Rmat, Rvec);
            rPNP.eigen2Mat(rt.t.transpose(), &Tmat_RPnP);
            poseEstimator.Project3Dpoints(I, "Projected Axis (RPnP)", points_3D_for_project, Rvec, Tmat_RPnP, "Window");
        }
        else window_corners_2D.clear();
    }

//    if(detect_door_and_window)
//    {

//        std::vector<cv::Point2f> window_corners_2D;
//        window_corners_2D = objectDetector.DetectWindowCorners(I_for_color);
//        std::vector<cv::Point3f> window_corners_3D = Read3DObjectPoints(window_corners_2D, 3);
//        poseEstimator.EstimatePoseFromPNP(window_corners_3D, window_corners_2D);
//        if(window_corners_2D.size() == 4)
//        {
//            std::vector<cv::Point3f> points_3D_for_project = Read3DProjectedPoints(3);
//            cv::Mat Rmat = poseEstimator.getOrientation();
//            cv::Mat Tmat = poseEstimator.getTranslation();
//            poseEstimator.Project3Dpoints(I, "Window Projected Axis", points_3D_for_project, Rmat, Tmat, "None");
//        }
//    }

    if((door_corners_2D.size() == 4) || (window_corners_2D.size() == 4)){

        /// Transform from Camera Frame of Reference (FoR) to Gazebo FoR
        ///
        ///
        // OpenCv PnP (from camera FoR to window FoR)
        Tmat = - Tmat;
        // RPnP (from camera FoR to window FoR)
        Tmat_RPnP = - Tmat_RPnP;
        // OpenCv PnP (from window FoR to Gazebo FoR)
        // Conversion for imav_indor.world
//        Mat Temp;
//        Tmat.copyTo(Temp);
//        Tmat.at<double>(0,0) = Temp.at<double>(0,0) + ORIGIN_WIN_GAZEBO_X;
//        Tmat.at<double>(1,0) = Temp.at<double>(2,0) + ORIGIN_WIN_GAZEBO_Y;
//        Tmat.at<double>(2,0) = - Temp.at<double>(1,0) + ORIGIN_WIN_GAZEBO_Z;

        // Conversion for iris_opt_flow.world
        Mat Temp;
        Tmat.copyTo(Temp);
        Tmat.at<double>(0,0) = Temp.at<double>(2,0) + ORIGIN_WIN_GAZEBO_X;
        Tmat.at<double>(1,0) = - Temp.at<double>(0,0) + ORIGIN_WIN_GAZEBO_Y;
        Tmat.at<double>(2,0) = - Temp.at<double>(1,0) + ORIGIN_WIN_GAZEBO_Z;

        // RPnP (from window FoR to Gazebo FoR)
        // Conversion for imav_indor.world
//        Tmat_RPnP.copyTo(Temp);
//        Tmat_RPnP.at<double>(0,0) = Temp.at<double>(0,0) + ORIGIN_WIN_GAZEBO_X;
//        Tmat_RPnP.at<double>(1,0) = Temp.at<double>(2,0) + ORIGIN_WIN_GAZEBO_Y;
//        Tmat_RPnP.at<double>(2,0) = - Temp.at<double>(1,0) + ORIGIN_WIN_GAZEBO_Z;

        // Conversion for iris_opt_flow.world
        Tmat_RPnP.copyTo(Temp);
        Tmat_RPnP.at<double>(0,0) = Temp.at<double>(2,0) + ORIGIN_WIN_GAZEBO_X;
        Tmat_RPnP.at<double>(1,0) = - Temp.at<double>(0,0) + ORIGIN_WIN_GAZEBO_Y;
        Tmat_RPnP.at<double>(2,0) = - Temp.at<double>(1,0) + ORIGIN_WIN_GAZEBO_Z;

        /// Publish poses
        ///

        geometry_msgs::Pose rpnp_pose;
        rpnp_pose.position.x = Tmat_RPnP.at<double>(0,0);
        rpnp_pose.position.y = Tmat_RPnP.at<double>(1,0);
        rpnp_pose.position.z = Tmat_RPnP.at<double>(2,0);

        RPnPpub.publish(rpnp_pose);

        geometry_msgs::Pose cvpnp_pose;
        cvpnp_pose.position.x = Tmat.at<double>(0,0);
        cvpnp_pose.position.y = Tmat.at<double>(1,0);
        cvpnp_pose.position.z = Tmat.at<double>(2,0);

        OpenCVPnPpub.publish(cvpnp_pose);
    }

    char tecla = cv::waitKey(1);
//    if(tecla==27)
//        break;
     if(tecla=='p')
        tecla=cv::waitKey();
    else if(tecla=='g')
    {
        //writer.write(I_ROIs_corners);
    }
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }
}

std::vector<cv::Point3f> Read3DObjectPoints(std::vector<cv::Point2f> &image_corners, int object_type)
{

    std::vector<cv::Point3f> points_3D;
    cv::Point3f p;

    switch(object_type)
    {
        case 1: //3D DOOR POINTS (with respect upper-left corner of the door GAZEBO)
            cout<<"Door"<<endl;
            points_3D.push_back(cv::Point3f(0.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(1.2, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(1.2, 2.0, 0.0));
            points_3D.push_back(cv::Point3f(0.0, 2.0, 0.0));
            break;
        case 2: //3D WINDOW POINTS (with respect upper-left corner of the door GAZEBO)
            cout<<"Window"<<endl;
            points_3D.push_back(cv::Point3f(6.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(7.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(7.0, 1.0, 0.0));
            points_3D.push_back(cv::Point3f(6.0, 1.0, 0.0));
            break;
        case 3: //3D WINDOW POINTS (with respect upper-left corner of the window GAZEBO)
            cout<<"Window"<<endl;
            points_3D.push_back(cv::Point3f(0.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(1.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(1.0, 1.0, 0.0));
            points_3D.push_back(cv::Point3f(0.0, 1.0, 0.0));
            break;
        case 4: //3D DOOR POINTS (Lab framework)
            cout<<"Door"<<endl;
            points_3D.push_back(cv::Point3f(0.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(0.135, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(0.135, 0.205, 0.0));
            points_3D.push_back(cv::Point3f(0.0, 0.205, 0.0));
            break;

        case 5: //3D WINDOW POINTS (Lab framework)
            cout<<"Window"<<endl;
            points_3D.push_back(cv::Point3f(0.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(0.11, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(0.11, 0.11, 0.0));
            points_3D.push_back(cv::Point3f(0.0, 0.11, 0.0));
            break;
        case 6: //3D DOOR AND WINDOW POINTS (with respect upper-left corner of the window GAZEBO)
            cout<<"Window and Door"<<endl;
            points_3D.push_back(cv::Point3f(0.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(1.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(1.0, 1.0, 0.0));
            points_3D.push_back(cv::Point3f(1.0, 1.0, 0.0));
            points_3D.push_back(cv::Point3f(-6.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(-4.8, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(-4.8, 2.0, 0.0));
            points_3D.push_back(cv::Point3f(-6.0, 2.0, 0.0));
            break;
        case 7: //3D DOOR AND WINDOW POINTS (with respect upper-left corner of the window Lab Framework)
            cout<<"Window and Door"<<endl;
            points_3D.push_back(cv::Point3f(0.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(0.105, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(0.105, 0.105, 0.0));
            points_3D.push_back(cv::Point3f(0.0, 0.105, 0.0));
            points_3D.push_back(cv::Point3f(-0.6, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(-0.47, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(-0.47, 0.205, 0.0));
            points_3D.push_back(cv::Point3f(-0.6, 0.205, 0.0));
            break;
        case 8: //3D DOOR POINTS (with respect upper-left corner of the window GAZEBO)
            cout<<"Door"<<endl;
            points_3D.push_back(cv::Point3f(-6.0, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(-4.8, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(-4.8, 2.0, 0.0));
            points_3D.push_back(cv::Point3f(-6.0, 2.0, 0.0));
            break;
        case 9: //3D DOOR POINTS (with respect upper-left corner of the window Lab Framework)
            cout<<"Window"<<endl;
            points_3D.push_back(cv::Point3f(-0.6, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(-0.47, 0.0, 0.0));
            points_3D.push_back(cv::Point3f(-0.47, 0.205, 0.0));
            points_3D.push_back(cv::Point3f(-0.6, 0.205, 0.0));
            break;
    }

    //cout<<"window_corners_3D:"<<endl;
    //for(int i=0;i<points_3D.size();i++)
    //	cout<<points_3D[i]<<endl;
    //cout<<endl<<endl;

    return points_3D;
}

std::vector<cv::Point3f> Read3DProjectedPoints(int object_type)
{
    std::vector<cv::Point3f> points_3D_for_project;
    cv::Point3f p;

    switch(object_type)
    {
        case 1: //3D DOOR POINTS (with respect upper-left corner of the door GAZEBO)
            cout<<"Door"<<endl;
            points_3D_for_project.push_back(cv::Point3f(0.6, 1.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(1.2, 1.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.6, 0.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.6, 1.0, 0.6));
            break;
        case 2: //3D WINDOW POINTS (with respect upper-left corner of the door GAZEBO)
            cout<<"Window"<<endl;
            points_3D_for_project.push_back(cv::Point3f(6.0, 0.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(7.0, 0.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(7.0, 1.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(6.0, 1.0, 0.0));
            break;
        case 3: //3D WINDOW POINTS (with respect upper-left corner of the window GAZEBO)
            cout<<"Window"<<endl;
            points_3D_for_project.push_back(cv::Point3f(0.5, 0.5, 0.0));
            points_3D_for_project.push_back(cv::Point3f(1.0, 0.5, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.5, 0.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.5, 0.5, 0.5));
            break;
        case 4: //3D DOOR POINTS (Lab framework)
            cout<<"Door"<<endl;
            points_3D_for_project.push_back(cv::Point3f(0.065, 0.105, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.135, 0.105, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.065, 0.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.065, 0.105, 0.065));
            break;
        case 5: //3D DOOR POINTS (Lab framework)
            cout<<"Door"<<endl;
            points_3D_for_project.push_back(cv::Point3f(-0.465, -0.295, 0.0));
            points_3D_for_project.push_back(cv::Point3f(-0.265, 0.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(-0.465, 0.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(-0.465, -0.295, 0.0));
            break;
        case 6: //3D WINDOW POINTS (Lab framework)
            cout<<"Door"<<endl;
            points_3D_for_project.push_back(cv::Point3f(0.0, 0.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.11, 0.0, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.11, 0.11, 0.0));
            points_3D_for_project.push_back(cv::Point3f(0.0, 0.11, 0.0));
            break;

    }

    return points_3D_for_project;
}
